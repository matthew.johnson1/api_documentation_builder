const fs = require('fs')
const https = require('https')
const path = require('path')

const AWS = require('aws-sdk')
const creds = process.env.creds ? JSON.parse(process.env.creds) : false

AWS.config.update({
  httpOptions: {
    agent: new https.Agent({
      rejectUnauthorized: true,
      ca: [fs.readFileSync(path.resolve(process.cwd(), './ca-bundle.crt'))]
    })
  },
  s3: {
    endpoint: `s3.${process.env.endpoint}`
  },
  lambda: {
    endpoint: `lambda.${process.env.endpoint}`,
    ...(creds && { ...creds.Credentials })
  },
  kms: {
    endpoint: `kms.${process.env.endpoint}`
  }
})

/**
 * Encapsulation function for s3.getObject
 * @param {String} Key file key
 */
async function getObject (Key) {
  try {
    const s3 = new AWS.S3()
    const reply = await s3.getObject({
      Key,
      Bucket: process.env.Bucket
    }).promise()
    return [null, reply.Body.toString()]
  } catch (err) {
    return [err]
  }
}

/**
 * Encapsulation function for lambda.invoke
 * @param {*} param0 Event object from trigger or invoke
 */
async function invoke ({ FunctionName, Payload }) {
  try {
    const lambda = new AWS.Lambda()
    const reply = await lambda.invoke({
      FunctionName,
      Payload,
      LogType: 'Tail'
    }).promise()
    return [null, JSON.parse(reply.Payload.toString())]
  } catch (err) {
    return [err]
  }
}

/**
 * Decrypts a given list of desired environment variables
 * @param {Array} vars A list of environment variables to be decrypted
 */
function kmsDecrypt (vars = []) {
  const kms = new AWS.KMS()
  return Promise.all(vars.map(val => {
    if (!(val in process.env)) {
      throw new Error(`${val} was required for decryption but did not exist in environment.`)
    }
    return kms.decrypt({ CiphertextBlob: Buffer.from(process.env[val], 'base64') }).promise()
  }))
}

module.exports = {
  getObject,
  invoke,
  kmsDecrypt
}
