// dependency included inside lambda runtime
const { kmsDecrypt } = require('./aws')

// These case-sensitive properties must be configured as C2S environment variables
const required = {
  Bucket: 'TSECM Documentation S3 bucket name',
  endpoint: 'Example: us-east-1.aws.com',
  roleId: 'Vault approle roleId',
  secretId: 'Vault approle secretId'
}

/**
 * Validates required environment variables listed in module
 */
function validateEnv () {
  Object.keys(required).map(value => {
    if (!(value in process.env)) throw new Error(`${value} not found in environment variables.`)
  })
}

/**
 * Decrypts and extracts desired encrypted environment variables
 * @param {*} vars Desired environment variables to be decrypted
 */
async function extractDecryptedEnv (vars = []) {
  try {
    const decrypted = await kmsDecrypt(vars)
    return decrypted.map(val => val.Plaintext.toString())
  } catch (err) {
    console.log(err)
    process.exit(1)
  }
}

module.exports = {
  extractDecryptedEnv,
  validateEnv
}
