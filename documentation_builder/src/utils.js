const fs = require('fs')
const https = require('https')
const path = require('path')

const { parsers } = require('../parser-index.json')

/**
 * Finds the corresponding lambda function name dependent on parserName
 * @param {String} parserName Parser name for the event object
 * @returns {String | null} String if there is a valid result otherwise null
 */
function findLambdaFunction (parserName) {
  return Object.keys(parsers).reduce((acc, curVal) => {
    if (parsers[curVal].functions.includes(parserName)) acc = parsers[curVal].functionName
    return acc
  }, null)
}

module.exports = {
  findLambdaFunction
}
