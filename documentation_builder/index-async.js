const AWS = require('aws-sdk')
const creds = process.env.creds ? JSON.parse(process.env.creds) : false
const fs = require('fs')
const https = require('https')
const path = require('path')

const batchSize = process.env.batchSize
const params = {
  Bucket: process.env.Bucket,
  StartAfter: process.env.StartAfter
}
let uniqArr = []

AWS.config.update({
  httpOptions: {
    agent: new https.Agent({
      rejectUnauthorized: true,
      ca: [fs.readFileSync(path.resolve(process.cwd(), './ca-bundle.crt'))]
    })
  },
  s3: {
    endpoint: `s3.${process.env.endpoint}`,
    httpOptions: {
      timeout: 120000
    }
  },
  lambda: {
    endpoint: `lambda.${process.env.endpoint}`,
    ...(creds && { ...creds.Credentials })
  },
  kms: {
    endpoint: `kms.${process.env.endpoint}`
  }
})

/**
 * Build Index.html from uniqArr
 */

function buildIndex () {
  const hdrText = '<h1>TSECM Documentation</h1>'

  const content = uniqArr.map(link => {
    return '<a href="' + link + '">' + link + '</a><br >'
  }).join('')

  const idxParams = {
    Bucket: process.env.Bucket,
    ContentType: 'text/html;charset-UTF-8',
    Body: hdrText + content,
    Key: 'index.html'
  }

  const s3 = new AWS.S3({ endpoint: `s3.${process.env.endpoint}`, apiVersion: '2006-03-01' })
  s3.upload(idxParams, function (err, data) {
    if (err) console.log(err, err.stack)
    else {
      console.log('Successfully added ' + idxParams.Key)
    }
  })
}

/**
 * Remove files from uniqArr
 */
function removeFiles () {
  const regexpFiles = /\.[0-9a-z]+$/i

  const fltrdArr = uniqArr.filter(word => {
    const test = regexpFiles.test(word)
    if (!test) {
      return true
    }
  })
  uniqArr = fltrdArr
}

/**
 * Find toplevel folders in documentation
 * @param {Object} folders to iterate over
 */

async function _parseFolders (folders) {
  const batchLen = Array.from(folders.Contents).length
  let idx = 0
  Array.from(folders.Contents).map((obj) => {
    idx++
    if (obj.Key) {
      const toplevFolder = obj.Key.split('/')[0]
      if (uniqArr.indexOf(toplevFolder) === -1) {
        uniqArr.push(toplevFolder)
      }
      if (idx === batchLen) {
        process.env.StartAfter = uniqArr[uniqArr.length - 1]
        if (batchLen === parseInt(batchSize)) {
          const s3 = new AWS.S3({ endpoint: `s3.${process.env.endpoint}`, apiVersion: '2006-03-01' })
          params.StartAfter = uniqArr[uniqArr.length - 1]
          s3.listObjectsV2(params, function (err, callback) {
            if (err) console.log(err, err.stack)
            else {
              _parseFolders(callback)
            }
          })
        } else {
          removeFiles()
          buildIndex()
        }
      }
    }
  })
}

/**
 * Find toplevel folders in the documentation S3 Bucket
 * @param {Object} params to iterate over
 */
async function _getS3Folders (params) {
  try {
    const s3 = new AWS.S3()
    const reply = await s3.listObjectsV2(params).promise()
    return reply
  } catch (err) {
    console.log(err, err.stack)
  }
}

async function eventHandler (event) {
  try {
    const folders = await _getS3Folders({ Bucket: process.env.Bucket, StartAfter: process.env.StartAfter })
    const parsedArr = await _parseFolders(folders)
    return parsedArr
  } catch (err) {
    console.log(err, err.stack)
  }
}

exports.handler = eventHandler
