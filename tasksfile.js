const { sh, cli } = require('tasksfile')
const zipdir = require('zip-dir')

function jsdoc (options) {
  const projectDefaults = {
    src: [
      'documentation_builder_lambda/*'
    ],
    config: './jsdoc-config.json',
    dest: 'docs',
    template: 'node_modules/ink-docstrap/template'
  }
  const customSrc = options.src || []
  const src = [...projectDefaults.src, ...customSrc]
  const config = options.config || projectDefaults.config
  const dest = options.dest || projectDefaults.dest
  const template = options.template || projectDefaults.template
  sh(`npx jsdoc ${src.join(' ')} -c ${config} -d ${dest} -t ${template}`)
}

function lambdazip (options) {
  zipdir('/copy/documentation_builder', { saveTo: 'documentation_builder.zip' }, (err) => { if (err) process.exit(1) })
  zipdir('./jsParsers', { saveTo: 'jsParsers.zip' }, (err) => { if (err) process.exit(1) })
}

cli({
  jsdoc,
  lambdazip
})
